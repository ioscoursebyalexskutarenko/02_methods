//
//  ANParrentClass.m
//  02_Methods
//
//  Created by Admin on 24.12.16.
//  Copyright © 2016 Nesynov. All rights reserved.
//

#import "ANParrentClass.h"

@implementation ANParrentClass

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"Instansce of parrent class is initialized");
    }
    return self;
}

+ (void) whoAreYou {
    NSLog(@"I am ANParrentClass, %@", self);
    
}

- (void) sayHello {
    NSLog(@"Parrent say Hello!, %@", self);
}
        
- (void) say:(NSString*) string {
    NSLog(@"%@", string);
}

- (void) say:(NSString*) string and:(NSString*) string2 {
    NSLog(@"%@, %@", string, string2);
}

- (void) say:(NSString*) string and:(NSString*) string2 andAfteThat:(NSString*) string3 {
    NSLog(@"%@, %@, %@", string, string2, string3);
}

- (NSString*) saySomeNumberString {
    
    return [NSString stringWithFormat:@"%@", [NSDate date]];
}

- (NSString*) saySomething {
    
    NSString* string = [self saySomeNumberString];
    
    return string;
}

@end
