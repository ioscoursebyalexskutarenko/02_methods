//
//  ANChildClass.m
//  02_Methods
//
//  Created by Admin on 24.12.16.
//  Copyright © 2016 Nesynov. All rights reserved.
//

#import "ANChildClass.h"

@implementation ANChildClass

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"Instansce of chield class is initialized");
    }
    return self;
}

- (NSString*) saySomeNumberString {
    
    return @"Something";
}

- (NSString*) saySomething {
    
    [self saySomeNumberString];
    
    return [self saySomeNumberString];
}



@end
