//
//  ANParrentClass.h
//  02_Methods
//
//  Created by Admin on 24.12.16.
//  Copyright © 2016 Nesynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANParrentClass : NSObject

+ (void) whoAreYou;

- (void) sayHello;
- (void) say:(NSString*) string;
- (void) say:(NSString*) string and:(NSString*) string2;
- (void) say:(NSString*) string and:(NSString*) string2 andAfteThat:(NSString*) string3;
- (NSString*) saySomething;

- (NSString*) saySomeNumberString;

@end
